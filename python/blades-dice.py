# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Blades in the Dark - dice probabilities"""

from itertools import product

D6_LARGEST: int = 6

def d6_faces() -> list[int]:
    return list(map(lambda x: x+1, list(range(D6_LARGEST))))

def result_line(title, value, total) -> str:
    percent: float = round(value / total * 100, 2)
    print('- {:<16}: {:>4} / {:>4} ( {:>5} % )'.format(title, value, total, percent))

def statistics(d6_count: int) -> None:
    combinations: list[tuple[int]] = list(product(d6_faces(), repeat=d6_count))
    combination_count: int = len(combinations)

    critical: int = 0
    complete_success: int = 0
    partial_success: int = 0
    failure: int = 0

    for element in combinations:
        element: list = list(sorted(element))
        if len(element) > 1 and element[-1] == D6_LARGEST and element[-2] == D6_LARGEST:
            critical += 1
            continue
        match max(element):
            case 6:
                complete_success += 1
            case 5 | 4:
                partial_success += 1
            case 3 | 2 | 1:
                failure += 1

    all_full_success: int = critical + complete_success
    all_success: int = all_full_success + partial_success

    print()
    print(f'{d6_count}d6')
    result_line('critical', critical, combination_count)
    result_line('complete success', complete_success, combination_count)
    result_line('partial success', partial_success, combination_count)
    result_line('failure', failure, combination_count)
    
    print()
    if d6_count > 1:
        result_line('all full success', all_full_success, combination_count)
    result_line('all success', all_success, combination_count)

def main() -> None:
    for d6_count in range(1, 5):
        statistics(d6_count)
        
if __name__ == '__main__':
    main()
